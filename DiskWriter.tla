---- MODULE DiskWriter ----

LOCAL INSTANCE Naturals
LOCAL INSTANCE FiniteSets
LOCAL INSTANCE TLC

\* Pids for identifying writer processes.
CONSTANT WriterPids
ASSUME IsFiniteSet(WriterPids)

\* Define symmetry of pids.
PidSymmetry == Permutations(WriterPids)

\* A special value distinct from other values we care about.
CONSTANT none
ASSUME none \notin WriterPids
ASSUME none \notin Nat

\* How big is the disk we're writing to.
CONSTANT max_lba
ASSUME max_lba \in Nat
ASSUME max_lba >= 1

----

(*--algorithm DiskWriter

variables
  \* Who holds the lock for each LBA.
  lba_locks = [ lba \in 1..max_lba |-> none ],
  \* Model the disk simply as a map of who most recently wrote each block.
  disk_last_writer = [ lba \in 1..max_lba |-> none ];

define
  \* Shorthand for all LBAs in a request.
  lbas(req) == req.lba..(req.lba + req.len - 1)
end define;

fair process Writer \in WriterPids
  variables
    \* The active request, or the latest request, if successful.
    req = none,
    \* Which locks (we think) we are holding.
    locks_taken = { },
    \* Which LBA we're processing.
    cur_lba = none;
begin

  \* First we must read the header off the wire.
  \* This is an unfair action because whether it happens is out of our control.
  ReadHeader:-
    \* Nondeterministically create a request.
    with new_req_lba \in 1..max_lba,
      new_req_len \in 1..max_lba
    do
      \* Ignore "invalid" requests.
      await new_req_lba + new_req_len =< max_lba + 1;
      \* Remember the request.
      req := [ lba |-> new_req_lba, len |-> new_req_len ];
      \* Initialize our LBA counter for later.
      cur_lba := 1;
    end with;

  \* Next we must lock the request LBAs.
  \* This is a strongly fair action, to model a fair mutex.
  LockLBAs:+
    \* So long as we haven't yet locked all LBAs...
    while \E lba \in lbas(req) \ locks_taken: TRUE
    do
      \* pick an LBA to lock,
      with lba \in lbas(req) \ locks_taken
      do
        \* but make sure it's the smallest (or else we can deadlock).
        await \A lba2 \in lbas(req) \ locks_taken: lba =< lba2;
        \* Wait for the lock to be free,
        await lba_locks[lba] = none;
        \* then take it,
        lba_locks[lba] := self;
        \* and remember that we did so.
        locks_taken := locks_taken \union {lba};
      end with;
    end while;

  \* Now we can read blocks from the wire, and write to disk.
  \* Normally reading would be an unfair action, but we
  \* enforce a timeout, so it is weakly fair.
  ReadBlock:
    \* So long as there's data left to read,
    while cur_lba < req.lba + req.len do
      either
        \* either "read" it,
        skip;
      or
        \* or time out.  When timing out, erase the request, so
        \* atomicity checking doesn't think it was a successful request.
        req := none;
        \* Make sure we release the locks regardless.
        goto UnlockLBAs;
      end either;

    WriteBlock:
      \* Now "write" the block.  Really we just record that we
      \* were the last process to touch it.
      disk_last_writer[cur_lba] := self;
      \* Move on to the next LBA.
      cur_lba := cur_lba + 1;

    end while;

  \* Finally we must unlock any LBAs we locked.
  UnlockLBAs:
    while \E lba \in locks_taken: TRUE
    do
      \* Order doesn't (shouldn't) matter here.
      with lba \in locks_taken
      do
        \* Sanity check.  (The LockOwnershipInvariant invariant should
        \* catch any real issues here.)
        assert lba_locks[lba] = self;
        \* Release the lock!
        lba_locks[lba] := none;
        \* And remember that we released it.
        locks_taken := locks_taken \ {lba};
      end with;
    end while;

    \* "Clean up" cur_lba to simplify the state space a bit.
    cur_lba := none;

    \* Go read another request!
    goto ReadHeader;

end process;

end algorithm*)
\* BEGIN TRANSLATION
VARIABLES lba_locks, disk_last_writer, pc

(* define statement *)
lbas(req) == req.lba..(req.lba + req.len - 1)

VARIABLES req, locks_taken, cur_lba

vars == << lba_locks, disk_last_writer, pc, req, locks_taken, cur_lba >>

ProcSet == (WriterPids)

Init == (* Global variables *)
        /\ lba_locks = [ lba \in 1..max_lba |-> none ]
        /\ disk_last_writer = [ lba \in 1..max_lba |-> none ]
        (* Process Writer *)
        /\ req = [self \in WriterPids |-> none]
        /\ locks_taken = [self \in WriterPids |-> { }]
        /\ cur_lba = [self \in WriterPids |-> none]
        /\ pc = [self \in ProcSet |-> "ReadHeader"]

ReadHeader(self) == /\ pc[self] = "ReadHeader"
                    /\ \E new_req_lba \in 1..max_lba:
                         \E new_req_len \in 1..max_lba:
                           /\ new_req_lba + new_req_len =< max_lba + 1
                           /\ req' = [req EXCEPT ![self] = [ lba |-> new_req_lba, len |-> new_req_len ]]
                           /\ cur_lba' = [cur_lba EXCEPT ![self] = 1]
                    /\ pc' = [pc EXCEPT ![self] = "LockLBAs"]
                    /\ UNCHANGED << lba_locks, disk_last_writer, locks_taken >>

LockLBAs(self) == /\ pc[self] = "LockLBAs"
                  /\ IF \E lba \in lbas(req[self]) \ locks_taken[self]: TRUE
                        THEN /\ \E lba \in lbas(req[self]) \ locks_taken[self]:
                                  /\ \A lba2 \in lbas(req[self]) \ locks_taken[self]: lba =< lba2
                                  /\ lba_locks[lba] = none
                                  /\ lba_locks' = [lba_locks EXCEPT ![lba] = self]
                                  /\ locks_taken' = [locks_taken EXCEPT ![self] = locks_taken[self] \union {lba}]
                             /\ pc' = [pc EXCEPT ![self] = "LockLBAs"]
                        ELSE /\ pc' = [pc EXCEPT ![self] = "ReadBlock"]
                             /\ UNCHANGED << lba_locks, locks_taken >>
                  /\ UNCHANGED << disk_last_writer, req, cur_lba >>

ReadBlock(self) == /\ pc[self] = "ReadBlock"
                   /\ IF cur_lba[self] < req[self].lba + req[self].len
                         THEN /\ \/ /\ TRUE
                                    /\ pc' = [pc EXCEPT ![self] = "WriteBlock"]
                                    /\ req' = req
                                 \/ /\ req' = [req EXCEPT ![self] = none]
                                    /\ pc' = [pc EXCEPT ![self] = "UnlockLBAs"]
                         ELSE /\ pc' = [pc EXCEPT ![self] = "UnlockLBAs"]
                              /\ req' = req
                   /\ UNCHANGED << lba_locks, disk_last_writer, locks_taken, 
                                   cur_lba >>

WriteBlock(self) == /\ pc[self] = "WriteBlock"
                    /\ disk_last_writer' = [disk_last_writer EXCEPT ![cur_lba[self]] = self]
                    /\ cur_lba' = [cur_lba EXCEPT ![self] = cur_lba[self] + 1]
                    /\ pc' = [pc EXCEPT ![self] = "ReadBlock"]
                    /\ UNCHANGED << lba_locks, req, locks_taken >>

UnlockLBAs(self) == /\ pc[self] = "UnlockLBAs"
                    /\ IF \E lba \in locks_taken[self]: TRUE
                          THEN /\ \E lba \in locks_taken[self]:
                                    /\ Assert(lba_locks[lba] = self, 
                                              "Failure of assertion at line 117, column 9.")
                                    /\ lba_locks' = [lba_locks EXCEPT ![lba] = none]
                                    /\ locks_taken' = [locks_taken EXCEPT ![self] = locks_taken[self] \ {lba}]
                               /\ pc' = [pc EXCEPT ![self] = "UnlockLBAs"]
                               /\ UNCHANGED cur_lba
                          ELSE /\ cur_lba' = [cur_lba EXCEPT ![self] = none]
                               /\ pc' = [pc EXCEPT ![self] = "ReadHeader"]
                               /\ UNCHANGED << lba_locks, locks_taken >>
                    /\ UNCHANGED << disk_last_writer, req >>

Writer(self) == ReadHeader(self) \/ LockLBAs(self) \/ ReadBlock(self)
                   \/ WriteBlock(self) \/ UnlockLBAs(self)

(* Allow infinite stuttering to prevent deadlock on termination. *)
Terminating == /\ \A self \in ProcSet: pc[self] = "Done"
               /\ UNCHANGED vars

Next == (\E self \in WriterPids: Writer(self))
           \/ Terminating

Spec == /\ Init /\ [][Next]_vars
        /\ \A self \in WriterPids : /\ WF_vars((pc[self] # "ReadHeader") /\ Writer(self))
                                    /\ SF_vars(LockLBAs(self))

Termination == <>(\A self \in ProcSet: pc[self] = "Done")

\* END TRANSLATION

----

\* The type invariants are very straightforward simple type checks.
GlobalTypeInvariant ==
  /\ lba_locks \in [ 1..max_lba -> WriterPids \union {none} ]
  /\ disk_last_writer \in [ 1..max_lba -> WriterPids \union {none} ]

WriterTypeInvariant ==
  \A pid \in WriterPids:
    /\ req[pid] \in ([ lba: Nat, len: Nat ] \union {none})
    /\ cur_lba[pid] \in (Nat \union {none})
    /\ locks_taken[pid] \in SUBSET Nat

\* Ensure that at any point in time, eventually a worker can
\* get around to reading a request header.
AnyWorkerServiceGuarantee ==
  []<>(\E pid \in WriterPids: ENABLED ReadHeader(pid))

\* Ensure that at any point in time, *all* workers eventually
\* can get around to reading a request header.
\* Implies AnyWorkerServiceGuarantee.
AllWorkersServiceGuarantee ==
  \A pid \in WriterPids: []<>(ENABLED ReadHeader(pid))

\* Ensure that each process really owns the locks it thinks it owns.
LockOwnershipInvariant ==
  \A pid \in WriterPids:
    \A lba \in locks_taken[pid]:
      lba_locks[lba] = pid

\* Ensure that requests are truly written atomically.  We do this by
\* confirming that,
AtomicityInvariant ==
  \* in any state where *all* workers completed their most recent
  \* request successfully,
  (\A pid \in WriterPids: pc[pid] = "ReadHeader" /\ req[pid] /= none) =>
    \* at least one of them
    \E pid \in WriterPids:
      \* must have been the last one to touch all the LBAs in its
      \* most recent request (and therefore not intermingled with
      \* a concurrent request).
      \A lba \in lbas(req[pid]): disk_last_writer[lba] = pid

====

Things to try:

* Don't take locks.  This should violate AtomicityInvariant.
* Steal locks.  This should violate LockOwnershipInvariant.
* Don't order lock taking.  This should result in a deadlock.
* Make LockLBAs weakly fair.  This should violate AllWorkersServiceGuarantee.
* Make ReadBlock unfair.  This should violate AnyWorkersServiceGuarantee.
* Go back to ReadHeader instead of UnlockLBAs on error.  This should result
  in a deadlock.
