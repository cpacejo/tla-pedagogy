<!DOCTYPE html [
  <!ENTITY i "&#xA0;&#xA0;">
  <!ENTITY qquad "&#x2003;&#x2003;">
]>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
  <head>
    <title>TLA⁺ tips</title>

    <style type="text/css"><![CDATA[
      html {
      	background-color: #174;
      }

      body {
        max-width: 6in;
        margin-left: auto;
        margin-right: auto;
        background-color: #fec;
        padding: 18pt;
        border: solid #111 1.75pt;
        border-radius: 7pt;
        color: #111;
      }

      body :first-child {
        margin-top: 0;
      }

      body :last-child {
        margin-bottom: 0;
      }

      h1, h2, h3, h4, h5, h6 {
        font-family: sans-serif;
      }

      h1::before { content: "[ "; }
      h1::after { content: " ]"; }

      h3::before { content: "❭ "; }
      h4::before { content: "❭❭ "; }
      h5::before { content: "❭❭❭ "; }
      h6::before { content: "❭❭❭❭ "; }

      p.code {
        border-left: solid #11c 1.25pt;
        padding-left: 0.5rem;
        white-space: nowrap;
        overflow-x: scroll;
      }

      code {
      	font-family: serif;
      }

      code.ascii {
        font-family: monospace;
      }

      span.kwd {
        text-transform: lowercase;
        font-variant: small-caps;
      }

      table.borders, table.borders td {
      	border: thin solid black;
      	border-collapse: collapse;
      }

      table.borders th, table.borders td {
      	padding: 0.3rem;
      }


      /* sequent calculus stuff */

      td.rule {
        text-align: center;
      }

      div.rule, div.rule-upper, div.rule-lower {
        display: inline-block;
        text-align: center;
      }

      div.rule hr, div.rule-upper hr, div.rule-lower hr {
        border: none;
        height: 1px;
        background-color: black;
        margin: 0;
      }

      div.rule {
        vertical-align: middle;
      }

      div.rule-upper {
        vertical-align: bottom;
      }

      div.rule-lower {
        vertical-align: top;
      }
    ]]></style>
  </head>

  <body>
    <h1>TLA<sup>+</sup> tips</h1>

    <h2>Use constraints</h2>

    <p>Rather than enforce hard resource limits (e.g., maximum queue depth),
      use <code>CONSTRAINT</code>s to enforce such limits.  Hard resource
      limits cause TLC to assume that certain transitions are not possible,
      leading to false violations of deadlock and temporal checking.  In
      contrast, constraints merely instruct TLC not to visit states which do
      not match the constraint, so deadlock and temporal properties are not
      falsely violated.  (Of course, true violations will not be reported
      for the states which are not visited.)</p>


    <h2>Use symmetry, but not too much</h2>

    <p>Declaring a <code>SYMMETRY</code> can drastically reduce the search
      space, but at the cost of checking the symmetry for every state
      generated.  For symmetries of more than 4 or 5 values, this check
      quickly becomes more costly than the brute‐force search itself. 
      Beyond 6 values, startup becomes quite costly as TLC computes the
      permutation set.  If you do have a large set whose symmetry you wish
      to exploit, instead, declare as the <code>SYMMETRY</code> the union of
      permutations of several sets which partition the large set.</p>

    <p>When using symmetries, don’t forget that you must never treat any
      member of the symmetry set specially.  <em>This includes using
      <code><span class="kwd">CHOOSE</span></code> to select an element from the set!</em>  Instead of
      <code><span class="kwd">CHOOSE</span></code>, use the existential operator (<code>∃</code>)
      to select an element.  However, if you are using <code><span class="kwd">CHOOSE</span></code>
      at the constant level, e.g. to select a fencepost or null value, it
      is better to simply exclude the chosen value from the symmetry set.</p>

    <p>Recognize also cases where nothing is gained from breaking symmetry. 
      One common case is with a pool of unique identifiers which are
      handed out during execution.  The pool should necessarily be finite
      so that a symmetry may be applied, but can be made to appear infinite
      by use of a <code>CONSTRAINT</code> which excludes states in which the
      pool is empty.  However, note that in this case, the final identifier
      handed out is never actually used, since the constraint causes the
      search to immediately terminate.  To reduce the size of the symmetry
      set, predetermine the identifier which will be the last to be handed
      out and exclude it from the symmetry set.</p>


    <h2>Check types using TLAPS</h2>

    <p>While TLC can be used to check type invariance, you can alternatively
      use TLAPS to more robustly verify such properties.  In addition, TLAPS
      can verify the types and semantics of your operators.</p>

    <p>For each module you wish to typecheck, create a separate module
      (perhaps in a special directory) which <code><span class="kwd">EXTEND</span></code>s it.  In
      this module, you will define the type invariant and prove type
      judgements.</p>

    <p>In the module being typechecked, you will need to name any
      <code><span class="kwd">ASSUME</span></code>s your proofs will need.  It’s also a good idea in
      the typechecking module to <code><span class="kwd">USE</span></code> these assumptions, and
      <code><span class="kwd">USE</span> <span class="kwd">DEF</span></code> the type invariant and any type definitions, so
      you do not need to do so repeatedly in the proofs.</p>

    <p>I find it makes sense to divide the proofs into five sections: basic
      typechecking, dependent typechecking, action type invariance, API type
      invariance, and operator semantics.</p>


    <h3>Basic typechecking</h3>

    <p>In this section, prove for each operator of the module, that given
      arguments of the expected types, it evalutes to a value of the
      expected type.  (For action operators, this is always
      <code><span class="kwd">BOOLEAN</span></code>.)</p>

    <p>These proofs will generally take the following form:</p>

    <p class="code"><code>
      <span class="kwd">THEOREM</span><br/>
      &i;<span class="kwd">ASSUME</span><br/>
      &i;&i;<var>TypeInvariant</var>,<br/>
      &i;&i;<span class="kwd">NEW</span> <var>arg1</var> ∈ <var>Arg1Type</var>, <span class="kwd">NEW</span> <var>arg2</var> ∈ <var>Arg2Type</var><br/>
      &i;<span class="kwd">PROVE</span><br/>
      &i;&i;<var>Op</var>(<var>arg1</var>, <var>arg2</var>) ∈ <var>OpType</var><br/>
      <span class="kwd">PROOF</span><br/>
      &i;<span class="kwd">BY</span> <span class="kwd">DEFS</span> <var>Op</var>, <var>TypeInvariant</var>
    </code></p>

    <p>These proofs may of course be parametric over one or more types,
      in which case you will need a <code><span class="kwd">NEW</span> <var>T</var></code> in the
      <code><span class="kwd">ASSUME</span></code> clause for each such type <var>T</var>.</p>


    <h3>Dependent typechecking</h3>

    <p>In this section, place any proofs which refine the type of each
      operator when applied to specific arguments, or under some other
      constraint.  You may have none or many; just define whichever refined
      type judgements you feel are worthwhile to the user of the module.</p>

    <p>These proofs will generally look something like this:</p>

    <p class="code"><code>
      <span class="kwd">THEOREM</span><br/>
      &i;<span class="kwd">ASSUME</span><br/>
      &i;&i;<var>TypeInvariant</var>,<br/>
      &i;&i;<span class="kwd">NEW</span> <var>arg1</var> ∈ <var>Arg1Type</var>, <span class="kwd">NEW</span> <var>arg2</var> ∈ <var>Arg2Type</var>,<br/>
      &i;&i;<var>SomeProperty</var>(<var>arg1</var>)<br/>
      &i;<span class="kwd">PROVE</span><br/>
      &i;&i;<var>Op</var>(<var>arg1</var>, <var>arg2</var>) ≠ <var>special_value</var><br/>
      <span class="kwd">PROOF</span><br/>
      &i;<span class="kwd">BY</span> <span class="kwd">DEFS</span> <var>Op</var>, <var>TypeInvariant</var>, <var>SomeProperty</var>, <var>special_value</var>
    </code></p>


    <h3>Action type invariance</h3>

    <p>In this section, prove that the action operators which comprise the
      module’s API maintain the module’s type invariant.  Most of these
      proofs will look like the following:</p>

    <p class="code"><code>
      <span class="kwd">THEOREM</span> <var>Action1TypeInvariance</var> ≜<br/>
      &i;<span class="kwd">ASSUME</span><br/>
      &i;&i;<var>TypeInvariant</var>,<br/>
      &i;&i;<span class="kwd">NEW</span> <var>arg1</var> ∈ <var>Arg1Type</var>, <span class="kwd">NEW</span> <var>arg2</var> ∈ <var>Arg2Type</var>,<br/>
      &i;&i;<var>Action1</var>(<var>arg1</var>, <var>arg2</var>)<br/>
      &i;<span class="kwd">PROVE</span><br/>
      &i;&i;<var>TypeInvariant</var>′<br/>
      <span class="kwd">PROOF</span><br/>
      &i;<span class="kwd">BY</span> <span class="kwd">DEFS</span> <var>Action1</var>, <var>TypeInvariant</var>
    </code></p>

    <p>You may also wish to include a proof that the module’s initialization
      operator establishes the type invariant (though usually this is sufficiently
      trivial that you may prove it inline in the proof of API type invariance).
      Such a proof will look like this:</p>

    <p class="code"><code>
      <span class="kwd">THEOREM</span> <var>InitTypeInvariance</var> ≜<br/>
      &i;<span class="kwd">ASSUME</span> <var>Init</var> <span class="kwd">PROVE</span> <var>TypeInvariant</var><br/>
      <span class="kwd">PROOF</span><br/>
      &i;<span class="kwd">BY</span> <span class="kwd">DEFS</span> <var>Init</var>, <var>TypeInvariant</var>
    </code></p>

    <p>You will want to name these theorems so they can be referred to in the
      following section.</p>


    <h3>API type invariance</h3>

    <p>Next, you will want to prove that type invariance is maintained when
      the module is used in accordance with its API.  This is what negates
      the need to check type invariance for this module with TLC.</p>

    <p>First, define a local operator <code>Next</code> which entails all
      possible applications of the action operators which comprise the 
      module’s API, as if you were defining a next‐state operator for TLC
      which was exercising the module:</p>

    <p class="code"><code>
      <span class="kwd">LOCAL</span> <var>Next</var> ≜<br/>
      &i;∨ ∃ <var>x</var> ∈ <var>T</var>, <var>y</var> ∈ <var>U</var>:<br/>
      &i;&i;&i;<var>Action1</var>(<var>x</var>, <var>y</var>)<br/>
      &i;∨ ∃ <var>x</var> ∈ <var>T</var>:<br/>
      &i;&i;&i;<var>Cond</var>(<var>x</var>) ∧ <var>Action2</var>(<var>x</var>)
    </code></p>

    <p>Next you can prove type invariance of ◻[<var>Next</var>]<sub><var>vars</var></sub>, using
      the action type invariance theorems you established earlier:</p>

    <p class="code"><code>
      <span class="kwd">THEOREM</span> <var>NextTypeInvariance</var> ≜<br/>
      &i;<span class="kwd">ASSUME</span><br/>
      &i;&i;<var>TypeInvariant</var>,<br/>
      &i;&i;◻[<var>Next</var>]<sub><var>vars</var></sub><br/>
      &i;<span class="kwd">PROVE</span><br/>
      &i;&i;<var>TypeInvariant</var>′<br/>
      <span class="kwd">PROOF</span><br/>
      &i;<span class="kwd">BY</span> <var>Action1TypeInvariance</var>, <var>Action2TypeInvariance</var><br/>
      &i;&i;<span class="kwd">DEFS</span> <var>Next</var>, <var>vars</var>
    </code></p>

    <p>Finally you can prove type invariance of the system as a whole, using
      temporal reasoning:</p>

    <p class="code"><code>
      <span class="kwd"><span class="kwd">LOCAL</span></span> <span class="kwd">INSTANCE</span> <var>TLAPS</var><br/>
      <span class="kwd">THEOREM</span><br/>
      &i;<var>Init</var> ∧ ◻[<var>Next</var>]<sub><var>vars</var></sub> ⇒ ◻<var>TypeInvariant</var><br/>
      <span class="kwd">PROOF</span><br/>
      &i;<span class="kwd">BY</span> <var>PTL</var>, <var>InitTypeInvariance</var>, <var>NextTypeInvariance</var><br/>
    </code></p>

    <p><code><var>PTL</var></code> is the propositional temporal logic proof tactic
      and is found in the built‐in <code><var>TLAPS</var></code> module.</p>

    <p>Note that it is important to represent temporal theorems as implications (⇒),
      rather than as sequents (<code><span class="kwd">ASSUME</span></code>/<code><span class="kwd">PROVE</span></code>).
      This is because, unlike with an implication, the antecedents (<code><span class="kwd">ASSUME</span></code> formulae) of a sequent
      automatically become hypotheses of the proof. When this happens, it prevents application
      of the Generalized Necessitation Rule (TLA<sup>+2</sup> Preliminary Guide section 8.3) which is necessary
      to infer global truths from constant truths.</p>


    <h3>Operator semantics</h3>

    <p>In this section you will want to prove that the state and action
      operators do what you intend them to do.  This is not possible in the
      general case, and will be difficult in many cases.  In such cases, you
      will want to instead check the desired properties for a finite domain
      using TLC (though you may of course still include the theorems here
      with an <code><span class="kwd">OMITTED</span></code> proof).</p>

    <p>The form of such proofs will vary dependening on the particular
      property, so no general form is given here.</p>

    <p>Note also that the current version of the TLA<sup>+</sup> Proof Manager
      has limited capability for reasoning about temporal statements, so
      verifying many of the more interesting properties of your operators
      may require the use of TLC even if the proof is conceptually simple.</p>


    <h2 id="proof-step-quick-reference">Proof step quick reference</h2>

    <p>The following tables summarize TLA<sup>+2</sup>’s proof steps using <a href="https://en.wikipedia.org/wiki/Sequent_calculus" rel="external">sequent calculus</a>.</p>

    <p>In all cases, a rule is only valid if its parenthesized portions are also valid (that is, can be readily proved via automated means).</p>

    <p>Any proof <var>Π</var> must be complete (that is, start with an inference line).</p>


    <p>Proofs:</p>

    <table class="borders">
      <tr>
        <th>Proof</th>
        <th>Rule</th>
      </tr>

      <tr>
        <td><code>(<span class="kwd">PROOF</span>) <span class="kwd">OBVIOUS</span></code></td>
        <td class="rule"><div class="rule">
          <hr/>
          (Γ ⊢ <var>G</var>)
        </div></td>
      </tr>

      <tr>
        <td><code>(<span class="kwd">PROOF</span>) <span class="kwd">OMITTED</span></code></td>
        <td class="rule"><div class="rule">
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code>(<span class="kwd">PROOF</span>) <span class="kwd">BY</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper"><hr/>(Γ ⊢ <var>F</var>)</div>&qquad;<div class="rule-upper"><hr/>(Γ, <var>F</var> ⊢ <var>G</var>)</div>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code>(<span class="kwd">PROOF</span>) <span class="kwd">BY</span> <span class="kwd">ONLY</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper"><hr/>(Γ ⊢ <var>F</var>)</div>&qquad;<div class="rule-upper"><hr/>(Γ′, <var>F</var> ⊢ <var>G</var>)</div>
          <hr/>
          Γ ⊢ <var>G</var>
        </div><br/>(Γ′ being Γ with all facts removed)</td>
      </tr>

      <tr>
        <td><code>(<span class="kwd">PROOF</span>) <span class="kwd">BY</span> <span class="kwd">DEF</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper"><hr/>(Γ, <var>F</var> ≜ <var>H</var> ⊢ <var>G</var>)</div>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code>(<span class="kwd">PROOF</span>) <span class="kwd">BY</span> <span class="kwd">ONLY</span> <span class="kwd">DEF</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper"><hr/>(Γ′, <var>F</var> ≜ <var>H</var> ⊢ <var>G</var>)</div>
          <hr/>
          Γ ⊢ <var>G</var>
        </div><br/>(Γ′ being Γ with all facts removed)</td>
      </tr>

      <tr>
        <td><code>
          (<span class="kwd">PROOF</span>)<br/>
          &i;<var>σ</var><sub>1</sub> <var>Σ</var><sub>1</sub><br/>
          &i;⋮<br/>
          &i;<var>σ</var><sub><var>n</var></sub> <var>Σ</var><sub><var>n</var></sub>
        </code></td>

        <td class="rule"><div class="rule">
          <div class="rule-upper"><var>Σ</var><sub><var>n</var></sub><br/>⋮<br/><var>Σ</var><sub>1</sub></div>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>
    </table>


    <p>Steps which take proofs:</p>

    <table class="borders">
      <tr>
        <th>Step</th>
        <th>Rule</th>
      </tr>

      <tr>
        <td><code><var>F</var><br/>&i;<var>Π</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper">(<var>Π</var>)<hr/>Γ ⊢ <var>F</var></div>&qquad;Γ, <var>F</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code>
          <span class="kwd">ASSUME</span> <var>H</var> <span class="kwd">PROVE</span> <var>F</var><br/>
          &i;<var>Π</var>
        </code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper">(<var>Π</var>)<hr/>Γ, <var>H</var> ⊢
            <var>F</var></div>&qquad;Γ, <code><span class="kwd">ASSUME</span> <var>H</var>
            <span class="kwd">PROVE</span> <var>F</var></code> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">CASE</span> <var>F</var><br/>&i;<var>Π</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper">(<var>Π</var>)
            <hr/>Γ, <var>F</var> ⊢ <var>G</var></div>&qquad;Γ, <code><span class="kwd">ASSUME</span> <var>F</var>
            <span class="kwd">PROVE</span> <var>G</var></code> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">SUFFICES</span> <var>F</var><br/>&i;<var>Π</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper">(<var>Π</var>)
            <hr/>Γ, <var>F</var> ⊢ <var>G</var></div>&qquad;Γ ⊢ <var>F</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code>
          <span class="kwd">SUFFICES</span> <span class="kwd">ASSUME</span> <var>H</var> <span class="kwd">PROVE</span> <var>F</var><br/>
          &i;<var>Π</var>
        </code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper">(<var>Π</var>)
            <hr/>Γ, <code><span class="kwd">ASSUME</span> <var>H</var> <span class="kwd">PROVE</span> <var>F</var></code>
            ⊢ <var>G</var></div>&qquad;Γ, <var>H</var> ⊢ <var>F</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">PICK</span> <var>x</var>: <var>F</var><br/>&i;<var>Π</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper">(<var>Π</var>)
            <hr/>Γ ⊢ ∃ <var>x</var>: <var>F</var></div>&qquad;Γ, <span class="kwd">NEW</span> <var>x</var>,
            <var>F</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">PICK</span> <var>x</var> ∈ <var>S</var>: <var>F</var><br/>&i;<var>Π</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper">(<var>Π</var>)
            <hr/>Γ ⊢ ∃ <var>x</var> ∈ <var>S</var>: <var>F</var></div>&qquad;Γ, <span class="kwd">NEW</span>
            <var>x</var> ∈ <var>S</var>, <var>F</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">QED</span><br/>&i;<var>Π</var></code></td>
        <td class="rule"><div class="rule">
          (<var>Π</var>)
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>
    </table>


    <p>Steps which do not take proofs:</p>

    <table class="borders">
      <tr>
        <th>Step</th>
        <th>Rule</th>
      </tr>

      <tr>
        <td><code>(<span class="kwd">DEFINE</span>) <var>F</var> ≜ <var>H</var></code></td>
        <td class="rule"><div class="rule">
          Γ, <var>F</var> ≜ <var>H</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">USE</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper"><hr/>(Γ ⊢ <var>F</var>)</div>&qquad;Γ, <var>F</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">USE</span> <span class="kwd">ONLY</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper"><hr/>(Γ ⊢ <var>F</var>)</div>&qquad;Γ′, <var>F</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div><br/>(Γ′ being Γ with all facts removed)</td>
      </tr>

      <tr>
        <td><code><span class="kwd">USE</span> <span class="kwd">DEF</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          Γ, <var>F</var> ≜ <var>H</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">USE</span> <span class="kwd">ONLY</span> <span class="kwd">DEF</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          Γ′, <var>F</var> ≜ <var>H</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>G</var>
        </div><br/>(Γ′ being Γ with all facts removed)</td>
      </tr>

      <tr>
        <td><code><span class="kwd">HIDE</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          Γ ⊢ <var>G</var>
          <hr/>
          Γ, <var>F</var> ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">HIDE</span> <span class="kwd">DEF</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          Γ ⊢ <var>G</var>
          <hr/>
          Γ, <var>F</var> ≜ <var>H</var> ⊢ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">HAVE</span> <var>F</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper"><hr/>(Γ ⊢ <var>H</var> ⇒ <var>F</var>)</div>&qquad;Γ, <var>F</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ <var>H</var> ⇒ <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">TAKE</span> <var>x</var></code></td>
        <td class="rule"><div class="rule">
          Γ, <span class="kwd">NEW</span> <var>x</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ ∀ <var>x</var>: <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">TAKE</span> <var>x</var> ∈ <var>S</var></code></td>
        <td class="rule"><div class="rule">
          Γ, <span class="kwd">NEW</span> <var>x</var> ∈ <var>S</var> ⊢ <var>G</var>
          <hr/>
          Γ ⊢ ∀ <var>x</var> ∈ <var>S</var>: <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">WITNESS</span> <var>e</var></code></td>
        <td class="rule"><div class="rule">
          Γ ⊢ <var>G</var>[<var>e</var>/<var>x</var>]
          <hr/>
          Γ ⊢ ∃ <var>x</var>: <var>G</var>
        </div></td>
      </tr>

      <tr>
        <td><code><span class="kwd">WITNESS</span> <var>e</var> ∈ <var>S</var></code></td>
        <td class="rule"><div class="rule">
          <div class="rule-upper"><hr/>(Γ ⊢ <var>e</var> ∈ <var>S</var>)</div>&qquad;Γ, <var>e</var> ∈ <var>S</var> ⊢ <var>G</var>[<var>e</var>/<var>x</var>]
          <hr/>
          Γ ⊢ ∃ <var>x</var> ∈ <var>S</var>: <var>G</var>
        </div></td>
      </tr>
    </table>

    <p>Note that most of the above steps also work with more than one
      argument, with the obvious extension of meaning.</p>


    <h2>Appendix</h2>

    <h3>ASCII Representation of Typeset Symbols</h3>

    <p>From the TLA<sup>+</sup> Summary hyperbook.</p>

    <table style="width: 100%;">
      <tr>
        <td><code>∧</code></td> <td><code class="ascii">/\</code> or <code class="ascii">\land</code></td>
        <td><code>∨</code></td> <td><code class="ascii">\/</code> or <code class="ascii">\lor</code></td>
        <td><code>⇒</code></td> <td><code class="ascii">=&gt;</code></td>
      </tr>

      <tr>
        <td><code>¬</code></td> <td><code class="ascii">~</code> or <code class="ascii">\lnot</code> or <code class="ascii">\neg</code></td>
        <td><code>≡</code></td> <td><code class="ascii">&lt;=&gt;</code> or <code class="ascii">\equiv</code></td>
        <td><code>≜</code></td> <td><code class="ascii">==</code></td>
      </tr>

      <tr>
        <td><code>∈</code></td> <td><code class="ascii">\in</code></td>
        <td><code>∉</code></td> <td><code class="ascii">\notin</code></td>
        <td><code>≠</code></td> <td><code class="ascii">#</code> or <code class="ascii">/=</code></td>
      </tr>

      <tr>
        <td><code>⟨</code></td> <td><code class="ascii">&lt;&lt;</code></td>
        <td><code>⟩</code></td> <td><code class="ascii">&gt;&gt;</code></td>
        <td><code>◻</code></td> <td><code class="ascii">[]</code></td>
      </tr>

      <tr>
        <td><code>&lt;</code></td> <td><code class="ascii">&lt;</code></td>
        <td><code>&gt;</code></td> <td><code class="ascii">&gt;</code></td>
        <td><code>◇</code></td> <td><code class="ascii">&lt;&gt;</code></td>
      </tr>

      <tr>
        <td><code>≤</code></td> <td><code class="ascii">\leq</code> or <code class="ascii">=&lt;</code> or <code class="ascii">&lt;=</code></td>
        <td><code>≥</code></td> <td><code class="ascii">\geq</code> or <code class="ascii">&gt;=</code></td>
        <td><code>⤳</code></td> <td><code class="ascii">~&gt;</code></td>
      </tr>

      <tr>
        <td><code>≪</code></td> <td><code class="ascii">\ll</code></td>
        <td><code>≫</code></td> <td><code class="ascii">\gg</code></td>
        <td><code>⇾⃰</code>*</td> <td><code class="ascii">-+-></code></td>
      </tr>

      <tr>
        <td><code>≺</code></td> <td><code class="ascii">\prec</code></td>
        <td><code>≻</code></td> <td><code class="ascii">\succ</code></td>
        <td><code>↦</code></td> <td><code class="ascii">|-></code></td>
      </tr>

      <tr>
        <td><code>⪯</code></td> <td><code class="ascii">\preceq</code></td>
        <td><code>⪰</code></td> <td><code class="ascii">\succeq</code></td>
        <td><code>÷</code></td> <td><code class="ascii">\div</code></td>
      </tr>

      <tr>
        <td><code>⊆</code></td> <td><code class="ascii">\subseteq</code></td>
        <td><code>⊇</code></td> <td><code class="ascii">\supseteq</code></td>
        <td><code>⋅</code></td> <td><code class="ascii">\cdot</code></td>
      </tr>

      <tr>
        <td><code>⊂</code></td> <td><code class="ascii">\subset</code></td>
        <td><code>⊃</code></td> <td><code class="ascii">\supset</code></td>
        <td><code>∘</code></td> <td><code class="ascii">\o</code> or <code class="ascii">\circ</code></td>
      </tr>

      <tr>
        <td><code>⊏</code></td> <td><code class="ascii">\sqsubset</code></td>
        <td><code>⊐</code></td> <td><code class="ascii">\sqsupset</code></td>
        <td><code>∙</code></td> <td><code class="ascii">\bullet</code></td>
      </tr>

      <tr>
        <td><code>⊑</code></td> <td><code class="ascii">\sqsubseteq</code></td>
        <td><code>⊒</code></td> <td><code class="ascii">\sqsupseteq</code></td>
        <td><code>⋆</code></td> <td><code class="ascii">\star</code></td>
      </tr>

      <tr>
        <td><code>⊢</code></td> <td><code class="ascii">|-</code></td>
        <td><code>⊣</code></td> <td><code class="ascii">-|</code></td>
        <td><code><!--○-->◯</code></td> <td><code class="ascii">\bigcirc</code></td>
      </tr>

      <tr>
        <td><code>⊨</code></td> <td><code class="ascii">|=</code></td>
        <td><code>⫤</code></td> <td><code class="ascii">=|</code></td>
        <td><code>∼</code></td> <td><code class="ascii">\sim</code></td>
      </tr>

      <tr>
        <td><code>→</code></td> <td><code class="ascii">-&gt;</code></td>
        <td><code>←</code></td> <td><code class="ascii">&lt;-</code></td>
        <td><code>≃</code></td> <td><code class="ascii">\simeq</code></td>
      </tr>

      <tr>
        <td><code>∩</code></td> <td><code class="ascii">\cap</code> or <code class="ascii">\intersect</code></td>
        <td><code>∪</code></td> <td><code class="ascii">\cup</code> or <code class="ascii">\union</code></td>
        <td><code>≍</code></td> <td><code class="ascii">\asymp</code></td>
      </tr>

      <tr>
        <td><code>⊓</code></td> <td><code class="ascii">\sqcap</code></td>
        <td><code>⊔</code></td> <td><code class="ascii">\sqcup</code></td>
        <td><code>≈</code></td> <td><code class="ascii">\approx</code></td>
      </tr>

      <tr>
        <td><code>⊕</code></td> <td><code class="ascii">(+)</code> or <code class="ascii">\oplus</code></td>
        <td><code>⊎</code></td> <td><code class="ascii">\uplus</code></td>
        <td><code>≅</code></td> <td><code class="ascii">\cong</code></td>
      </tr>

      <tr>
        <td><code>⊖</code></td> <td><code class="ascii">(-)</code> or <code class="ascii">\ominus</code></td>
        <td><code>×</code></td> <td><code class="ascii">\X</code> or <code class="ascii">\times</code></td>
        <td><code>≐</code></td> <td><code class="ascii">\doteq</code></td>
      </tr>

      <tr>
        <td><code>⊙</code></td> <td><code class="ascii">(.)</code> or <code class="ascii">\odot</code></td>
        <td><code>≀</code></td> <td><code class="ascii">\wr</code></td>
        <td><code><var>x</var><sup><var>y</var></sup></code></td> <td><code class="ascii"><var>x</var>^<var>y</var></code></td>
      </tr>

      <tr>
        <td><code>⊗</code></td> <td><code class="ascii">(\X)</code> or <code class="ascii">\otimes</code></td>
        <td><code>∝</code></td> <td><code class="ascii">\propto</code></td>
        <td><code><var>x</var><sup>+</sup></code></td> <td><code class="ascii"><var>x</var>^+</code></td>
      </tr>

      <tr>
        <td><code>⊘</code></td> <td><code class="ascii">(/)</code> or <code class="ascii">\oslash</code></td>
        <td><code>“s”</code></td> <td><code class="ascii">"s"</code></td>
        <td><code><var>x</var><sup>∗</sup></code></td> <td><code class="ascii"><var>x</var>^*</code></td>
      </tr>

      <tr>
        <td><code>∃</code></td> <td><code class="ascii">\E</code></td>
        <td><code>∀</code></td> <td><code class="ascii">\A</code></td>
        <td><code><var>x</var><sup>#</sup></code></td> <td><code class="ascii"><var>x</var>^#</code></td>
      </tr>

      <tr>
        <td><code><b>∃</b></code></td> <td><code class="ascii">\EE</code></td>
        <td><code><b>∀</b></code></td> <td><code class="ascii">\AA</code></td>
        <td><code>′</code></td> <td><code class="ascii">'</code></td>
      </tr>

      <tr>
        <td><code>]<sub><var>v</var></sub></code></td> <td><code class="ascii">]_<var>v</var></code></td>
        <td><code>⟩<sub><var>v</var></sub></code></td> <td><code class="ascii">&gt;&gt;_<var>v</var></code></td>
      </tr>

      <tr>
        <td><code>WF<sub><var>v</var></sub></code></td> <td><code class="ascii">WF_<var>v</var></code></td>
        <td><code>SF<sub><var>v</var></sub></code></td> <td><code class="ascii">SF_<var>v</var></code></td>
      </tr>
    </table>

    <table style="width: 100%;">
      <tr>
        <td><code>┌───────</code></td> <td><code class="ascii">--------</code></td>
        <td><code>───────┐</code></td> <td><code class="ascii">--------</code></td>
      </tr>

      <tr>
        <td><code>├──────┤</code></td> <td><code class="ascii">--------</code></td>
        <td><code>└──────┘</code></td> <td><code class="ascii">========</code></td>
      </tr>

    </table>

    <p>* no exact Unicode equivalent</p>
  </body>
</html>
